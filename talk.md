# Talk Structure

## Who?

- Matt Davies
- 27 years in the Video Game Industry
- Senior Engineer for Rocket Science 
- Specialise in Engine design and Software Architecture

## What?

- What is ECS?
- How Bevy implements it using `bevy_ecs` crate
- Live code examples

## What Not?

- Not a talk about the Bevy Engine
- Not covering `bevy_app` crate - that's for a second talk

## Before ECS (part 1)

- Object Oriented
- Problems with hierarchies, where to derive from?
- Not scaleable

## Before ECS (part 2)

- Moved on to composition
- Independently invented "Behaviours" for "The Thing 2"
- Each behaviour is ticked
- Still mixes code and data
- Unity uses this system with Game Objects and Mono-Behaviours
- Unreal uses similar system with Actors and Actor Components
- But not fast enough for huge amounts of objects/actors

## ECS solution

- Promises
    - Fast and cache efficient
    - Code and data decoupled (data can be reinterpreted)
    - Automatically parallel

- Bevy Promises
    - Really easy to use (unlike Unity's DOTs and Unreal's Mass)

## What is ECS?

- **E**ntities
- **C**omponents
- **S**Systems

## Entities

- Just containers that contain components

## Components

- Contain ONLY data (series of fields)
- Stored efficiently
- Attached to an entity
- Each entity can only have one instance of a component

## Systems

- A system is code that acts on a list of components
- Each system queries all entities that contain certain components
- The system can then write and read to and from fields in components
- Can query the entities that the components belong to

## Bevy ECS

- All of the above
- Components are JUST Rust structs
- Systems are JUST Rust functions
- Supporting players:
    - World
    - Schedule
    - Resources
    - Command Lists
    - Component Bundles


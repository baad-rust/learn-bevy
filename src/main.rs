#![allow(unused)]

use bevy_ecs::prelude::*;

#[derive(Debug, Component)]
struct Position {
    x: f32,
    y: f32,
}

#[derive(Debug, Component)]
struct Velocity {
    x: f32,
    y: f32,
}

#[derive(Debug, Component)]
struct Player;

#[derive(Debug, Resource)]
struct GameTime(f32);

#[derive(Debug, Resource)]
struct CanMove(bool);

#[derive(Debug, Bundle)]
struct PlayerBundle {
    position: Position,
    velocity: Velocity,
    player: Player,
}

#[derive(SystemSet, Hash, Debug, Eq, PartialEq, Clone)]
enum SystemSets {
    Show,
}

fn main() {
    let mut world = World::new();

    let mut player1 = world.spawn_empty();
    player1.insert(PlayerBundle {
        position: Position { x: 1.0, y: 2.0 },
        velocity: Velocity { x: 1.0, y: 1.0 },
        player: Player,
    });

    let player2 = world.spawn(PlayerBundle {
        position: Position { x: 3.0, y: 4.0 },
        velocity: Velocity { x: 2.0, y: 2.0 },
        player: Player,
    });

    let monster1 = world.spawn((Position { x: 5.0, y: 6.0 }, Velocity { x: 3.0, y: 3.0 }));

    world.insert_resource(GameTime(0.1));
    world.insert_resource(CanMove(false));

    let mut schedule = Schedule::new();
    schedule.add_systems((
        (show_players, show_monsters, show_things)
            .chain()
            .in_set(SystemSets::Show),
        move_creatures.before(SystemSets::Show).run_if(can_move),
        debug_players.after(move_creatures),
    ));

    schedule.run(&mut world);
    schedule.run(&mut world);
}

fn show_players(query: Query<(&Position, &Velocity), With<Player>>) {
    for (pos, vel) in &query {
        println!("Player pos: {:?}, vel: {:?}", pos, vel);
    }
}

fn show_monsters(query: Query<(&Position, &Velocity), Without<Player>>) {
    for (pos, vel) in &query {
        println!("Monster pos: {:?}, vel: {:?}", pos, vel);
    }
}

fn show_things(query: Query<&Position>) {
    for pos in &query {
        println!("Thing pos: {:?}", pos);
    }
}

fn move_creatures(
    mut query: Query<(&mut Position, &Velocity)>,
    game_time: Res<GameTime>,
    mut commands: Commands,
    mut counter: Local<u32>,
) {
    for (mut pos, vel) in &mut query {
        pos.x += vel.x * game_time.0;
        pos.y += vel.y * game_time.0;
    }
    commands.spawn((
        Position { x: 7.0, y: 8.0 },
        Velocity { x: 4.0, y: 4.0 },
        Player,
    ));
    *counter += 1;
    println!("counter: {}", *counter);
}

fn debug_players(query: Query<(&Position, Entity), (Changed<Position>, With<Player>)>) {
    for (pos, entity) in &query {
        println!("Player {entity:?} moved to {pos:?}");
    }
}

fn can_move(can_move: Res<CanMove>) -> bool {
    can_move.0
}
